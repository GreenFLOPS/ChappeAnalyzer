#!/bin/bash

docker build -t greenflops/pipes_redis .
docker run -d --network="host" greenflops/pipes_redis
