//########################################################
//#       Copyright (C) 2017 GreenFLOPS                  #
//#                                                      #
//# This file is part of GreenFLOPS projects and can not #
//# be copied and/or distributed in any medium or format #
//#    without the express permission of GreenFLOPS      #
//#                                                      #
//#              contact@greenflops.com                  #
//########################################################


//cassandra requirement
var cassandra = require('cassandra-driver');

//kafka requirement
var kafka = require('kafka-node');
var Consumer = kafka.Consumer;
var Offset = kafka.Offset;
var Client = kafka.Client;
var topic = 'IPMI_CHANNEL';
var client = new Client('lecube.ai:2181'); // 2182 port du serveur kafka attention !
var redis = require("redis")
  , subscriber = redis.createClient(6379,'lecube.ai');

var http = require('http');
var path = require('path');
var fs = require('fs');
var server = http.createServer(handleRequest).listen(8080);

function handleRequest(request, response) {
    //set the base path so files are served relative to index.html
    var basePath = "public";
    var filePath = basePath + request.url;

    var contentType = 'text/html';
    var extname = path.extname(filePath);
    //get right Content-Type
    switch (extname) {
        case '.js':
            contentType = 'text/javascript';
            break;
        case '.css':
            contentType = 'text/css';
            break;
    }

    //default to index.html
    if (filePath == basePath + "/") {
        filePath = filePath + "index.html";
    }

    //Write the file to response
    fs.readFile(filePath, function(error, content) {
        if (error) {
            response.writeHead(500);
            response.end();
        } else {
            response.writeHead(200, {'Content-Type': contentType});
            response.end(content, 'utf-8');
        }
    });
}

// Chargement de socket.io
var io = require('socket.io').listen(server);

// Quand un browser se connecte
io.sockets.on('connection', function (socket) {

    socket.on('DISPATCH', (dest, jsonText) => {
        var i;
        for (i = 0; i < dest.length; i++) {
            console.log(dest[i]);
            bladeConnector.emit(dest[i], jsonText);
        };
    })

    socket.emit('connection_status', 'Vous êtes bien connecté !');

    socket.on('getHosts', () => {
	    io.sockets.emit('updatehosts', hosts);
    })

    socket.on('STOP', (msg) => {
        bladeConnector.emit('STOP', msg);
    });
    
});
server.listen(8080);



//
// Blade Management
//

var hosts = [];

function hostnameAdd(hostname){
	for(var i = 0; i < hosts.length; i++){
                console.log(i + " = " + hosts[i]);
        };
	if (hosts.includes(hostname)) {
    		return;
	}
	console.log('Add '+ hostname + ' to list');
	hosts.push(hostname.toString());
	for(var i = 0; i < hosts.length; i++){
		console.log(i + " = " + hosts[i]);
        };
	io.sockets.emit('updatehosts', hosts);
}

function hostnameDel(hostname){
	hosts.splice(hosts.indexOf(hostname), 1);
	console.log('Del '+ hostname + ' from list');
        for(var i = 0; i < hosts.length; i++){
                console.log(i + " = " + hosts[i]);
        };
	io.sockets.emit('updatehosts', hosts);
}


//
// blade Management  sur le port 1111
//

var bladeConnectorApp = require('http').createServer(bladeConnectorHandler)
var bladeConnectorio = require('socket.io')(bladeConnectorApp);
var bladeConnector = bladeConnectorio.of('/blade');

bladeConnectorApp.listen(1111);

function bladeConnectorHandler(req, res) {
}


bladeConnector.on('connection', function (socket) {
    console.log('new connection');

    socket.on('DISCOVER', function(hostname, rsp) {
        socket.hostname = hostname;
        console.log('recv discover from ' + hostname);
        hostnameAdd(hostname);
        rsp(0, '');
    })

    socket.on('disconnect', () => {
        console.log('disconnect from ' + socket.hostname);
        hostnameDel(socket.hostname);
    })

});


var topics = [{topic: 'IPMI_CHANNEL', partition: 0}];
var options = { autoCommit: false, fetchMaxWaitMs: 1000, fetchMaxBytes: 1024 * 1024 };
var consumer = new Consumer(client, topics, options);
var offset = new Offset(client);
var cassClient = new cassandra.Client({ contactPoints: ['lecube.ai']});
var queryKeyspace = "CREATE KEYSPACE IF NOT EXISTS demo WITH replication =" +
        "{'class': 'SimpleStrategy', 'replication_factor': '1' }";

var queryTable = "CREATE TABLE IF NOT EXISTS demo.ipmi " +
         "(timestamp timestamp, timestampSnd timestamp, timestampRcv timestamp, hostname varchar, temperature varchar, fan varchar, powerSupply varchar, PRIMARY KEY (hostname, timestamp)) WITH CLUSTERING ORDER BY (timestamp DESC)";

cassClient.execute(queryKeyspace);
setTimeout(function() {
  cassClient.execute(queryTable);
}, 5000);

console.log('Ready !');

subscriber.on("message", function(channel, message) {
    var timestampRcv = Date.now();
    console.log(timestampRcv + ": Message '" + message + "' on channel '" + channel + "' arrived!")
    io.sockets.emit('message', message, timestampRcv); //send data to draw charts on client browser

    var jsonContent = JSON.parse(message);

    var index;
    var TempCPU = '';
    var Fan = '';
    var Power_Supply = '';
    var timestamp = jsonContent.timestamp;
    var timestampSnd = jsonContent.timestampSnd;
    var hostname = jsonContent.hostname;
    var ipmioutput = jsonContent.value;
    var lines = jsonContent.value.split(";");

    for (index = 0; index < lines.length; ++index) {
        // console.log(lines[index]);
        var str = lines[index].toLowerCase();

        if (str.indexOf("cpu") >= 0 && str.indexOf("temp") >= 0 && str.indexOf("ok") >= 0) {
            TempCPU = str.split(",")[1];
            // console.log(TempCPU);
        }
        if (str.indexOf("cpu") >= 0 && str.indexOf("fan") >= 0 && str.indexOf("ok") >= 0) {
            Fan = str.split(",")[1];
            // console.log(Fan);
        }
        if (str.indexOf("power") >= 0 && str.indexOf("ok") >= 0) {
            Power_Supply = str.split(",")[1];
            // console.log(Power_Supply);
        }
    }

    var query = "insert into demo.ipmi (timestamp, timestampSnd, timestampRcv, hostname, temperature, fan, powerSupply) values (?, ?, ?, ?, ?, ? ,?)";
    cassClient.execute(query, [timestamp, timestampSnd, timestampRcv, hostname, TempCPU, Fan, Power_Supply], { prepare: true});

});

subscriber.subscribe("IPMI");


/*consumer.on('message', function (message) {
    var timestampRcv = new Date().toISOString();
    io.sockets.emit('message', message, timestampRcv); //send data to draw charts on client browser

	console.log(message.value.toString());
    var jsonContent = JSON.parse(message.value);

    var index;
    var TempCPU = '';
    var Fan = '';
    var Power_Supply = '';
    var timestamp = jsonContent.timestamp;
    var timestampSnd = jsonContent.timestampSnd;
    var hostname = jsonContent.hostname;
    var ipmioutput = jsonContent.value;
    var lines = jsonContent.value.split(";");

    for (index = 0; index < lines.length; ++index) {
        // console.log(lines[index]);
        var str = lines[index].toLowerCase();

        if (str.indexOf("cpu") >= 0 && str.indexOf("temp") >= 0 && str.indexOf("ok") >= 0) {
            TempCPU = str.split(",")[1];
            // console.log(TempCPU);
        }
        if (str.indexOf("cpu") >= 0 && str.indexOf("fan") >= 0 && str.indexOf("ok") >= 0) {
            Fan = str.split(",")[1];
            // console.log(Fan);
        }
        if (str.indexOf("power") >= 0 && str.indexOf("ok") >= 0) {
            Power_Supply = str.split(",")[1];
            // console.log(Power_Supply);
        }
    }

    var query = "insert into demo.ipmi (timestamp, timestampSnd, timestampRcv, hostname, temperature, fan, powerSupply) values (?, ?, ?, ?, ?, ? ,?)";
    cassClient.execute(query, [timestamp, timestampSnd, timestampRcv, hostname, TempCPU, Fan, Power_Supply], { prepare: true});

});

consumer.on('error', function (err) {
  console.log('error', err);
});*/

/*
* If consumer get `offsetOutOfRange` event, fetch data from the smallest(oldest) offset
*/
consumer.on('offsetOutOfRange', function (topic) {
  topic.maxNum = 2;
  offset.fetch([topic], function (err, offsets) {
    if (err) {
      return console.error(err);
    }
    var min = Math.min(offsets[topic.topic][topic.partition]);
    consumer.setOffset(topic.topic, topic.partition, min);
  });
});



