var socket = io.connect('http://lecube.ai:8080');

//manage hosts list
var selectionList = document.getElementById("hostList");
var testSensors = document.getElementById("testSensors");

var device;

var hosts = [];
var seriesList;

socket.emit('getHosts', '');

socket.on('updatehosts', function (hostname) {
    console.log("updating....")

    selectionList.innerHTML = '';

    for (var i = 0; i < hostname.length; i++) {
        option = document.createElement("option");
        option.text = hostname[i];
        console.log("option added : " + option.text);
        selectionList.options.add(option);
    }

    console.log("updated !");
})

var startButton = document.getElementById('startButton');
startButton.onclick = function () {

    // console.log(JSON.parse(jsonText).selectedList);
    // console.log(JSON.parse(jsonText).selectedList[0].value.toString());

    var selectionList = document.getElementById("hostList");
    console.log("number of selected devices : " + selectionList.selectedOptions.length);
    var destList = [];
    for (var i = 0; i < selectionList.selectedOptions.length; i++) {
        destList.push(selectionList.selectedOptions[i].innerText);
    }

    var settings = {
        sensor: sensors.options[sensors.selectedIndex].text,
        interval: document.getElementsByName('interval')[0].value,
        counts: document.getElementsByName('nombreMesure')[0].value
    };

    var jsonText = JSON.stringify(settings);
    console.log("Selected sensor : " + JSON.parse(jsonText).sensor);
    console.log("Selected interval: " + JSON.parse(jsonText).interval); //retrieve values from JSON Object
    console.log("Selected counts : " + JSON.parse(jsonText).counts);

    if (confirm("Send request to " + selectionList.selectedOptions.length + " device(s) ?")) {
        socket.emit('DISPATCH', destList, jsonText);
        window.location.href = "#charts";
    }
}

var stopButton = document.getElementById('stopButton');
stopButton.onclick = function () {
    socket.emit('STOP', 'arret du monitoring');
    console.log("STOP requested");
}

function getSelection() {
    device = selectionList.options[selectionList.selectedIndex].text;
    console.log(device);
}

var testButton = document.getElementById('testButton');
testButton.onclick = function () {
    var destList = [];
    for (var i = 0; i < testSensors.selectedOptions.length; i++) {
        destList.push(testSensors.selectedOptions[i].innerText);
        console.log(destList[i]);
    }

    var settings = {
            sensor: 'STRESS',
            duration: document.getElementsByName('testDuration')[0].value,
    };
    
    var jsonText = JSON.stringify(settings);

    if (confirm("Send test to " + testSensors.selectedOptions.length + " device(s) ?")) {
        socket.emit('DISPATCH', destList, jsonText);
    }

}

var chart = Highcharts.chart('container', {
    chart: {
        type: 'line',
        zoomType: 'x',
        animation: false,

    },

    /*scrollbar: {
        enabled: true,
    },*/

    plotOptions: {
        series: {
            showInNavigator: true,
            animation: {
                duration: 100,
            }
        }

    },

    navigator: {
        enabled: true,
        adaptToUpdatedData: true,
    },

    title: {
        text: 'Monitoring devices :'
    },

    credits: { enabled: false },

    xAxis: {
        type: 'datetime',
        


    },
    yAxis: [{
        // Temp yAxis
        labels: {
            format: '{value} C',
            style: {
                color: Highcharts.getOptions().colors[3]
            }
        },
        title: {
            text: 'Temperature',
            style: {
                color: Highcharts.getOptions().colors[3]
            }
        },
        opposite: true

    },

    { // RPM yAxis
        gridLineWidth: 0,
        title: {
            text: 'Rotation Speed',
            style: {
                color: Highcharts.getOptions().colors[1]
            }
        },
        labels: {
            format: '{value} rpm',
            style: {
                color: Highcharts.getOptions().colors[1]
            }
        }
    }],

    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) + '<br/>' +
                Highcharts.numberFormat(this.y, 2);
        }
    },

    exporting: {
        enabled: false
    },

    series: [],
});



socket.on('message', function (message, timestampRcv) {

    var jsonContent = JSON.parse(message.value);
    var lines = jsonContent.value.split(";");

    var index;
    var TempCPU = '';
    var Fan = '';
    var Power_Supply = '';
    var timestamp = new Date(jsonContent.timestamp);
    var title = "Monitoring device(s) : ";
    var hostname = JSON.parse(message.value).hostname;

    if (hosts.indexOf(hostname) === -1){ //if hostname doesn't exist already we create a new serie
    console.log("entré");    
    hosts.push(hostname);
        chart.addSeries({
            id: hostname + 'Temp',
            name: hostname + " CPU Temp",
            data: [],
            adaptToUpdatedData: true,
            //color: Highcharts.getOptions().colors[3],
            yAxis: 0,
        }); 

        chart.addSeries({
            id: hostname + 'Fan',
            name: hostname + " CPU Fan",
            data: [],
            yAxis: 1,
            //color: Highcharts.getOptions().colors[1],
        });

        console.log("Series ajoutées");

        testOption = document.createElement("option");
        testOption.text = hostname;
        testSensors.options.add(testOption);

    }else{

    } console.log("This host  already exists in list");

    for (var i = 0; i < hosts.length; i++) {
        title += hosts[i] + " ";
    }
    document.getElementById("titleChart").innerHTML = title;  //title


    for (index = 0; index < lines.length; ++index) {
        //console.log(lines[index]);
        var str = lines[index].toLowerCase();

        if (str.indexOf("cpu") >= 0 && str.indexOf("temp") >= 0 && str.indexOf("ok") >= 0) {
            TempCPU = str.split(",")[1];
            //console.log(TempCPU);
        }
        if (str.indexOf("cpu") >= 0 && str.indexOf("fan") >= 0 && str.indexOf("ok") >= 0) {
            Fan = str.split(",")[1];
            //console.log(Fan);
        }
        if (str.indexOf("power") >= 0 && str.indexOf("ok") >= 0) {
            Power_Supply = str.split(",")[1];
            //console.log(Power_Supply);
        }
    }

    var TempCPU = parseInt(TempCPU, 10);
    var Fan = parseInt(Fan, 10);

    var timestampSnd = new Date(jsonContent.timestampSnd);
    var tsRcv = new Date(timestampRcv);

    document.getElementById("json").innerHTML = JSON.stringify(message, undefined, 2);

    document.getElementById("pingIPMI").innerHTML = "Hardware Sensors Latency : " + (timestampSnd.getTime() - timestamp.getTime()) + " ms";
    document.getElementById("pingSend").innerHTML = "SDL Pipelines Latency : " + (tsRcv.getTime() - timestampSnd.getTime()) + " ms";

    console.log(typeof(chart.series));
    //chart.series[1].addPoint([timestamp.getTime(), TempCPU], true, true, );
    if (chart.get(hostname + 'Temp').data.length <= 200){ // after 60 values, highcharts stop adding points and replace old ones
        chart.get(hostname + 'Temp').addPoint([timestampSnd.getTime(), TempCPU], true, false); // timestampSn replace timestamp due to problems
        chart.get(hostname + 'Fan').addPoint([timestampSnd.getTime(), Fan], true, false);
    }

    else{
        chart.get(hostname + 'Temp').addPoint([timestampSnd.getTime(), TempCPU], true, true); // timestampSn replace timestamp due to problems
        chart.get(hostname + 'Fan').addPoint([timestampSnd.getTime(), Fan], true, true);
    }
    
    

});



