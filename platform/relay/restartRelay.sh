#!/bin/bash

# interactive docker run -it -p 8080:8080 -p 1111:1111 -v /home/monitor/ChappeAnalyzer/platform/relay/public:/app/public greenflops/relay /bin/bash

pid=$(docker ps | grep "greenflops/relay" | awk '{print $1}')
docker stop $pid
docker rm $pid

docker build -t greenflops/relay .

#docker run -dt --log-driver none -p 8080:8080 -p 1111:1111 -v /home/monitor/ChappeAnalyzer/platform/relay/public:/app/public greenflops/relay
docker run -it -p 8080:8080 -p 1111:1111 -v /home/monitor/ChappeAnalyzer/platform/relay/public:/app/public greenflops/relay

# You can attach to container with
# docker exec -it $(docker ps | grep "greenflops/relay" | awk '{print $1}') /bin/bash

