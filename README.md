ENHANCEMENTS:
- user interface: webui
- handle multiple devices
- handle (SDL) multiple pipeline systems
- handle generic schema datasets in JSON payloads

BUGS:
- Too big difference in Timestamps from same function & docker & machine

Health Cloud Analyzer is a platform to monitor performances and health like Temperature, Fan speed, Power consumption of cloud devices.

<pre>
.
├── README.md
├── devices
│   ├── 
└── platform
    ├── deeplearning
    │   └── 
    ├── pipelines
    │   └── 
    ├── relay
    │   ├── 
    └── storage
        └── 
</pre>

- devices directory contains sensors programs IPMItool, powerAPI, that runs on devices: blades, UE, ...
- platform contains files related to plaform management: user interface, webui, data storage, device communications, deep learning analytics
- pipelines contains framework for communications between devices and platforms: sdl, kafka, redis, Zmq, socketio
