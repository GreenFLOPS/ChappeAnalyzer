#!/bin/bash

# interactive: docker run -it --device /dev/ipmi0:/dev/ipmi0 -h pizzabox greenflops/sensors /bin/bash

if [ "$#" -ne 1 ]; then
    echo "Missing number of pizzabox to start"
    exit -1
fi

docker stop $(docker ps -a | grep sensors | awk '{print $1}')
docker rm $(docker ps -a | grep sensors | awk '{print $1}')

docker build -t greenflops/sensors .

for ((i=1; i<=$1; i++)); do
    echo "Start pizzabox$i"
    # docker run -dt --log-driver none --device /dev/ipmi0:/dev/ipmi0 -h pizzabox$i greenflops/sensors
    docker run -dt --device /dev/ipmi0:/dev/ipmi0 -h pizzabox$i greenflops/sensors
done
