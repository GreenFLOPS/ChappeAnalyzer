########################################################
#       Copyright (C) 2017 GreenFLOPS                  #
#                                                      #
# This file is part of GreenFLOPS projects and can not #
# be copied and/or distributed in any medium or format #
#    without the express permission of GreenFLOPS      #
#                                                      #
#              contact@greenflops.com                  #
########################################################


#!/bin/bash
ipmitool -c sdr elist full | grep -E 'Temperature|FAN|PMBPower' | tr '\n' ';'

