//########################################################/
//#       Copyright (C) 2017 GreenFLOPS                  #
//#                                                      #
//# This file is part of GreenFLOPS projects and can not #
//# be copied and/or distributed in any medium or format #
//#    without the express permission of GreenFLOPS      #
//#                                                      #
//#              contact@greenflops.com                  #
//########################################################



var pizzabox = require('socket.io-client').connect('http://lecube.ai:1111/blade', {
    reconnection: true,
    reconnectionDelay: 1000,
    reconnectionDelayMax : 5000,
    reconnectionAttempts: 99999
});

var os = require("os");
var hostname = os.hostname();
var loopIPMI;

pizzabox.on(hostname, (data) => {
    console.log(hostname + ' ' + data.toString() + ' received');
    if("IPMI" === JSON.parse(data).sensor){
        interval = 1000 * parseInt(JSON.parse(data).interval);
        count = parseInt(JSON.parse(data).counts);
        console.log('count: ' + count + ' interval: ' + interval + 'ms');
        rets=0;
        loopIPMI = setInterval(run, interval);
    }
    else if("POWERAPI" === JSON.parse(data).sensor){
        console.log('Start PowerAPI.. todo...');
        const { exec } = require('child_process');
        var duration = parseInt(JSON.parse(data).interval) * parseInt(JSON.parse(data).counts) / 1000;
        exec('cd powerapi-cli && /bin/bash ./bin/powerapi modules sigar-cpu-simple monitor --all --agg max duration ' + duration, (error, stdout, stderr) => {
            if (error) {
                console.error(`exec error: ${error}`);
                return;
            }
            console.log(`stdout: ${stdout}`);
            console.log(`stderr: ${stderr}`);
        });
    }
    else if("STRESS" === JSON.parse(data).sensor){
        console.log('Start stressing ' + hostname);
        const { exec } = require('child_process');
        exec('stress -c 12 -t ' + JSON.parse(data).duration, (error, stdout, stderr) => {
                if (error) {
                        console.error(`exec error: ${error}`);
                        return;
                }
                console.log(`stdout: ${stdout}`);
                console.log(`stderr: ${stderr}`);
        });
    }
    else if("STOP" === JSON.parse(data).sensor){
        console.log('STOP device ' + hostname);
        clearInterval(loopIPMI);
    }
})

pizzabox.on('STOP', () => {
    console.log('STOP All devices');
    clearInterval(loopIPMI);
})

pizzabox.on('connect', () => {
    console.log('send discover');
    pizzabox.emit('DISCOVER', hostname, (resp) => {
         console.log('server discover rsp: ' + resp);
    })
})

var kafka = require('kafka-node');
var Producer = kafka.Producer;
var KeyedMessage = kafka.KeyedMessage;
var Client = kafka.Client;
var client = new Client('lecube.ai:2181');
var topic = 'IPMI_CHANNEL';
var producer = new Producer(client, { requireAcks: 1 });
var rets = 0;
var os = require("os");
var count = 10;
var interval = 1000;
var redis = require("redis")
  , publisher = redis.createClient(6379,'lecube.ai');


producer.on('error', function (err) {
  console.log('error', err);
});

function run() {
    var message = '{\"timestamp\":\"' + Date.now() + '\"';
    var spawn = require('child_process').spawn;
    var command = spawn('./ipmi.sh');
    
    command.stdout.on('data', function(data) {
        message += ',\"hostname\":\"' + os.hostname() + '\",\"value\":\"' + data.toString() + '\"';
    });
    command.on('close', function(code) {
        message += ',\"timestampSnd\":\"' + Date.now() + '\"}';
        send(message);
    });
}

function send (message) {
    if((-1 === count) || (rets < count)){
        rets++;
	publisher.publish("IPMI",message);
       /* producer.send([
        {topic: topic, messages: [message]}
        ], function (err, data) {
                if (err) console.log(err);
        })*/
    }
    else {
        console.log('sent %d messages', rets);
        clearInterval(loopIPMI);
    }
}

